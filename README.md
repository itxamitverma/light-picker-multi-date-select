# Litepicker with Multiselect

This project demonstrates how to use Litepicker with multiselect functionality in an HTML document.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Introduction

This HTML file showcases the integration of Litepicker with multiselect capabilities. It allows users to select multiple dates and displays the selected dates in an input field.

## Features

- **Multiselect Functionality:** Enable the selection of multiple dates using Litepicker.
- **Apply and Cancel Buttons:** Utilize the "Apply" and "Cancel" buttons in the Litepicker footer to manage selected dates.
- **Date Formatting:** Display selected dates in the input field in a formatted way.

## Installation

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/itxamitverma/light-picker-multi-date-select
    ```

2. Open the HTML file in a web browser.

## Usage

1. Open the HTML file in a web browser.
2. Click on the input field to open the Litepicker calendar.
3. Select multiple dates using the multiselect feature.
4. Click the "Apply" button to update the input field with the selected dates.
5. Optionally, click the "Cancel" button to cancel the selection and update the input field.

## Contributing

If you'd like to contribute to this project, follow these steps:

1. Fork the project.
2. Create a new branch.
3. Make your changes and commit them.
4. Push to your fork and submit a pull request.

## License

This project is licensed under the [License Name] - see the [LICENSE.md](LICENSE.md) file for details.
